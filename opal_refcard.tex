\documentclass[10pt,landscape, a4paper]{article}
\usepackage{amssymb,amsmath,amsthm,amsfonts}
\usepackage{multicol,multirow}
\usepackage{enumitem}
\setlist{leftmargin=5mm, nosep}
\usepackage{calc}
\usepackage{ifthen}
\usepackage{graphicx}
\usepackage[landscape]{geometry}
\usepackage[colorlinks=true,citecolor=blue,linkcolor=blue]{hyperref}

\newcommand{\opaloption}[2]{\texttt{#1}\\
	\vspace{0.1cm}	
	#2
	\vspace{3mm}
	\hrule
}

\newcommand{\opaldefinition}[1]{\fbox{\parbox{\columnwidth}{\texttt{#1}}}}

\ifthenelse{\lengthtest { \paperwidth = 11in}}
    { \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
	{\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
	}
\pagestyle{empty}
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%x
                                {\normalfont\Large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                {-1explus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%
                                {\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {1ex plus .2ex}%
                                {\normalfont\small\bfseries}}

\makeatother
\setcounter{secnumdepth}{0}
\setlength{\parindent}{0pt}
\setlength{\parskip}{5pt plus 0.5ex}
% -----------------------------------------------------------------------

\title{OPAL Refcard}

\begin{document}

\raggedright
\footnotesize

\begin{center}
     \Huge{\textbf{OPAL Refcard}}\\
     \tiny{Last change: \today} \\
\end{center}
\begin{multicols*}{3}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}
\section{Disclaimer}
All the information and code in this document has been taken
from the OPAL wiki and/or files provided by Dr. A. Adelmann.

\textbf{Authors (alphabetically):}
\begin{itemize}
	\item Arnau Albà
	\item Renato Bellotti
\end{itemize}
\vspace{0.1cm}
\hrule
\section{General usage}
\includegraphics[width = .2\textwidth]{opal_scheme.png}\\
{\large Run with:\\
\texttt{mpirun /path/to/opal/opal input.in}}
\section{Input File Commands}
\subsection{Constants}
Constants can be defined in 2 ways:
\begin{itemize}
	\item As variables:\\
		\texttt{REAL rf\_freq = 1300.0;}
	\item In a separate data file (optimizer and sampler runs only!)\\
		\begin{itemize}
			\item How to set values in a \texttt{.data} file:\\
				\texttt{Npart 5000}
			\item How to use those values:\\
				\texttt{REAL n\_particles = \_Npart\_;}
		\end{itemize}
\end{itemize}
\vfill\null
\columnbreak
\subsection{Elements}
\opaldefinition{label:elementType, attribute,..., attribute}\\
\textbf{Examples:}
\begin{itemize}[label={--}]
\item \texttt{QF: QUADRUPOLE, L=1.8, K1=0.015832; }
\item \texttt{Bend: RBend, K0 = -0.0350195,
         FMAPFN = "fieldmap.txt",}
 \end{itemize}
\subsection{Line}

\opaldefinition{label:LINE=(member,...,member);}

Each member may be one of the following:
\begin{itemize}
	\item An element label
	\item A beam line label
	\item A sub-line, enclosed in parentheses
\end{itemize}
Beam lines can be nested to any level.

\subsection{Distribution}
\opaldefinition{Name:DISTRIBUTION, TYPE = DISTRIBUTION\_TYPE,\\
\hspace{1cm} ATTRIBUTE1 = value1,\\
\hspace{1cm} ...
}

Available distributions:
\begin{itemize}
	\item \texttt{FROMFILE}
	\item \texttt{GAUSS}
	\item \texttt{FLATTOP}
	\item \texttt{BINOMIAL}
	\item \texttt{GAUSSMATCHED}
\end{itemize}

Important attribute: \texttt{EMITTED}
\begin{itemize}
	\item FALSE: (default)\\
		The distribution is injected into the
simulation in its entirety at the start of the simulation.\\
		Note that in \texttt{OPAL-t} the
entire distribution will automatically be shifted to ensure that the
$z$ coordinate will be greater than zero for all particles.
	\item TRUE:\\
		The distribution is emitted into the simulation over time as the simulation progresses. Currently only \texttt{OPAL-t} supports this type of distribution.
\end{itemize}
\textbf{Example:}\\
\texttt{Dist: DISTRIBUTION, TYPE = FLATTOP,\\
        SCALABLE = TRUE,\\
		SIGMAX = \_SIGXY\_,		// Radius of laser (m) \\
		SIGMAY = \_SIGXY\_, \\
		TRISE = 1.0745e-12,	// (s) \\
		TFALL = 1.0745e-12,	// See notes above for equation of TRISE  \\
		TPULSEFWHM = \_FWHM\_ * 1.0e-12,	// FWHM length in time (s) \\
		CUTOFFLONG = 4.0,\\
		NBIN = 9,\\
		EMISSIONSTEPS = 100,\\
		EMISSIONMODEL = NONEQUIL,\\
		EKIN = 0.2,\\
		ELASER = 5.0,\\
		W = 3.2,\\
		FE = 3.2,\\
		CATHTEMP = 321.95,\\
		EMITTED = True,\\
		WRITETOFILE = False;}

\subsection{Fieldsolver}
\textbf{Example:}

\texttt{// The mesh sizes should be a factor of 2\\
// for most efficient space charge (SC) calculation.\\
FS\_SC: Fieldsolver, FSTYPE = FFT,\\
\hspace{3cm} MX = \_MX\_, MY = \_MY\_, MT = \_MT\_,\\
\hspace{3cm} PARFFTX = true, \\
\hspace{3cm} PARFFTY = true, \\
\hspace{3cm} PARFFTT = false,\\
\hspace{3cm} BCFFTX = open, \\
\hspace{3cm} BCFFTY = open, \\
\hspace{3cm} BCFFTT = open,\\
\hspace{3cm} BBOXINCR = 1, \\
\hspace{3cm} GREENSF = INTEGRATED;
}

\subsection{Track}
Before starting to track, a beam line and a beam must be selected. The time step \texttt{DT} and the maximal steps to track \texttt{MAXSTEPS} or \texttt{ZSTOP} should be set.\\
This command causes OPAL to enter "tracking mode", in which it accepts only the track commands see Table Commands accepted in Tracking Mode. \\
In order to perform several tracks, specify arrays of parameter in \texttt{DT}, \texttt{MAXSTEPS} and \texttt{ZSTOP}. This can be used to change the time step manually.

\textbf{Example:}

\texttt{TRACK, LINE = DRIVE,\\
\hspace{2cm} BEAM = BEAM1,\\
\hspace{2cm} MAXSTEPS = 1900000,\\
\hspace{2cm} DT = \{1.0e-13, \_DT\_\},\\
\hspace{2cm} ZSTOP=\{0.3, zend\};
}

Start / continue actual tracking:

\texttt{RUN, METHOD = "PARALLEL-T",\\
\hspace{2cm} BEAM = BEAM1,\\
\hspace{2cm} FIELDSOLVER = FS\_SC,\\
\hspace{2cm} DISTRIBUTION = Dist;}

\vfill\null\columnbreak
\subsection{Options}
\opaloption{OPTION, STATDUMPFREQ = n;}{How often beam stats dumped to .stat.}
\opaloption{OPTION, PSDUMPFREQ = n;}{Defines after how many time steps the phase space is dumped into the H5hut file.\\
Default: $10$}
\opaloption{OPTION, REPARTFREQ = n;}{
Defines after how many time steps we do particles repartition to
balance the computational load of the compute nodes.\\
Default: $10$}
\opaloption{OPTION, ECHO = TRUE / FALSE;}{Controls printing of an echo of input lines on the standard error
file.}
\opaloption{OPTION, INFO = TRUE / FALSE;}{If this option is turned off, OPAL suppresses all information messages.\\
It also affects the \texttt{gnu.out} and \texttt{eb.out} files in case of \texttt{OPAL-cycl} simulations.}
\opaloption{OPTION, TRACE = TRUE / FALSE;}{If \texttt{true}, print execution trace. Default \texttt{false}.}
\opaloption{OPTION, WARN = TRUE / FALSE;}{If this option is turned off, OPAL suppresses all warning messages.}
\opaloption{OPTION, TELL = TRUE / FALSE;}{If \texttt{true}, the current settings are listed. Must be the last option in the inputfile in order to render correct results.}
\opaloption{OPTION, SEED = int;}{Selects a particular sequence of random values.\\
A \texttt{SEED} value is an integer in the range $[0, ..., 999999999]$.\\
\texttt{SEED} can be an expression.\\
If \texttt{SEED === -1}, the time is used as seed
and the generator is not portable anymore.\\
Default: $123456789$}

\vfill\null\columnbreak
\subsection{Sampler}
Example \texttt{.in} file:

\begin{verbatim}
	OPTION, INFO=TRUE;

// Design variables
nstep:  DVAR, VARIABLE="nstep",
              LOWERBOUND=10,
              UPPERBOUND=40;
MX:     DVAR, VARIABLE="MX",
              LOWERBOUND=16,
              UPPERBOUND=32;

// Sampling methods
SM1: SAMPLING, VARIABLE="nstep",
               TYPE="FROMFILE",
               FNAME="samples.dat";
SM2: SAMPLING, VARIABLE="MX",
               TYPE="UNIFORM_INT",
               SEED=122,
               N = 6;

SAMPLE,
    RASTER          = false,
    DVARS           = {nstep, MX},
    SAMPLINGS       = {SM1, SM2},
    INPUT           = "Ring.tmpl",
    OUTPUT          = "RingSample",
    OUTDIR          = "RingSample",
    TEMPLATEDIR     = "template",
    FIELDMAPDIR     = "Fieldmaps",
    NUM_MASTERS     = 1,
    NUM_COWORKERS   = 1;
QUIT;
\end{verbatim}

And the template file \texttt{Ring.tmpl}:

\begin{verbatim}
Title,string="OPAL-cycl:
	the first turn acceleration in PSI 590MeV Ring";

REAL Edes=.072;
REAL gamma=(Edes+PMASS)/PMASS;
REAL beta=sqrt(1-(1/gamma^2));
REAL gambet=gamma*beta;
REAL P0 = gamma*beta*PMASS;
REAL brho = (PMASS*1.0e9*gambet) / CLIGHT;

REAL phi01=139.4281;
REAL volt1st=0.9;

REAL turns = 1;
REAL nstep=_nstep_;

REAL frequency=50.650;

ring: Cyclotron, TYPE="RING",
                 CYHARMON=6,
                 PHIINIT=0.0,
                 PRINIT=-0.000174,
                 RINIT=2130.0,
                 SYMMETRY=8.0,
                 RFFREQ=frequency,
                 FMAPFN="s03av.nar";

rf0: RFCavity, VOLT=volt1st,
               FMAPFN="Cav1.dat",
               TYPE="SINGLEGAP",
               FREQ=frequency,
               RMIN = 1900.0,
               RMAX = 4500.0,
               ANGLE=35.0,
               PDIS = 416.0,
               GAPWIDTH = 220.0,
               PHI0=phi01;

l1:   Line = (ring,rf0);

Dist1:DISTRIBUTION, TYPE=gauss,
                    sigmax = 2.0e-03,
                    sigmapx = 1.0e-7,
                    corrx = 0.0,
                    sigmay = 2.0e-03,
                    sigmapy = 1.0e-7,
                    corry = 0.0,
                    sigmat = 2.0e-03,
                    sigmapt = 3.394e-4,
                    corrt=0.0;

Fs1:FIELDSOLVER, FSTYPE=FFT,
                 MX=_MX_, MY=16, MT=16,
                 PARFFTX=true, PARFFTY=true, PARFFTT=true,
                 BCFFTX=open, BCFFTY=open, BCFFTT=open,
                 BBOXINCR=2;

beam1: BEAM, PARTICLE=PROTON,
             pc=P0, NPART=8192, BCURRENT=1.0E-3,
             CHARGE=1.0, BFREQ= frequency;

Select, Line=l1;

TRACK, LINE=l1, BEAM=beam1,
                MAXSTEPS=nstep*turns,
                STEPSPERTURN=360,
                TIMEINTEGRATOR="RK-4";

run, method = "CYCLOTRON-T",
     beam=beam1, fieldsolver=Fs1, distribution=Dist1;
endtrack;

Stop;
\end{verbatim}
The following sampling methods are available:
\begin{itemize}
	\item \texttt{FROMFILE}
	\item \texttt{UNIFORM}
	\item \texttt{UNIFORM\_INT}
	\item \texttt{GAUSSIAN}
	\item \texttt{LATIN\_HYPERCUBE}
	\item \texttt{RANDOM\_SEQUENCE\_UNIFORM\_INT}
	\item \texttt{RANDOM\_SEQUENCE\_UNIFORM}
\end{itemize}

\includegraphics[width=\columnwidth]{differenceSEQUENCESampler.png}

\vfill\null\columnbreak
\subsection{Optimizer}

Example \texttt{.in} file:

\begin{verbatim}
OPTION, ECHO=FALSE;
OPTION, INFO=TRUE;

TITLE, STRING="OPAL Test MAB, 2016-10-13";

REAL up = 0.0000977;
REAL loc = 2.0;

dv0: DVAR, VARIABLE="QDX1_K1",
           LOWERBOUND=0,
           UPPERBOUND=35;
dv1: DVAR, VARIABLE="QDX2_K1",
           LOWERBOUND=0,
           UPPERBOUND=34;
dv2: DVAR, VARIABLE="QFX1_K1",
           LOWERBOUND=-35,
           UPPERBOUND=0;

drmsx:   OBJECTIVE, EXPR="fabs(statVariableAt('rms_x', ${loc}) - ${up})";
drmsy:   OBJECTIVE, EXPR="fabs(statVariableAt('rms_y', ${loc}) - 0.0001833)";
goalfun: OBJECTIVE, EXPR="statVariableAt('rms_x', 2.00)";

OPTIMIZE, INPUT="tmpl/05-DL_QN3.tmpl",
          OBJECTIVES = {drmsx, drmsy, goalfun},
          DVARS = {dv0, dv1, dv2},
          INITIALPOPULATION=5,
          MAXGENERATIONS=3,
          NUM_IND_GEN=3,
          MUTATION_PROBABILITY=0.43,
          NUM_MASTERS=1,
          NUM_COWORKERS=1,
          SIMTMPDIR="simtmpdir",
          TEMPLATEDIR="tmpl",
          FIELDMAPDIR="fieldmaps",
          OUTPUT="optLinac",
          OUTDIR="results";

QUIT;
\end{verbatim}

\end{multicols*}

\end{document}